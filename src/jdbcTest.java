import java.sql.*;

public class jdbcTest{
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/";
		String dbName = "imdb";
		String driver = "com.mysql.jdbc.Driver";
		String userName = "root"; 
		String password = "12345678";

		try {
			Class.forName(driver);
			Connection conn = DriverManager.getConnection(url+dbName,userName,password);
			System.out.println("Connected to MySQL");
		
			try {
				Statement st = conn.createStatement();
				ResultSet rs = st.executeQuery("SELECT gender, count(*) FROM person GROUP BY gender");
				while (rs.next()) {
					System.out.println(rs.getString("gender")+": "+rs.getInt(2));
				}
				
				st.executeUpdate("DROP TABLE IF EXISTS JDBCtest");
				st.executeUpdate("CREATE TABLE JDBCtest(id int, string varchar(10))");
				st.executeUpdate("INSERT INTO JDBCtest VALUES (1,\"Tada!\")");				
		  	}
		  	catch(SQLException s){
		  		System.out.println(s.toString());
		  	}
		
			conn.close();
			System.out.println("Disconnected from MySQL");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

/*
JDBC syntax examples:

conn.setAutoCommit(false); // Disable automatic commit after each update
conn.commit(); // Commit all pending updates
conn.rollback(); // Abort all pending updates

PreparedStatement insertPerson = 
conn.prepareStatement("INSERT INTO person VALUES (?,?,?,?,?,?)"); // Create prepared statement
insertPerson.setInt(1, 123456);
insertPerson.setString(2, "John Doe");
insertPerson.setString(3, "M");
insertPerson.setDate(4, new java.sql.Date(160617600000)); // Set date, given in miliseconds since 1970-01-01
insertPerson.setNull(6,java.sql.Types.INTEGER); // Set to NULL
insertPerson.executeUpdate(); // Execute prepared statement with current parameters

Statement stmt = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
stmt.setFetchSize(Integer.MIN_VALUE); // Make sure result is read incrementally from MySQL

*/
