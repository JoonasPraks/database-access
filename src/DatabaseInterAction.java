import java.net.SocketOption;
import java.sql.*;
import java.util.Scanner;

/**
 * Created by pl on 16/11/2016.
 */
public class DatabaseInterAction {

    String url = "jdbc:mysql://localhost:3306/";
    String dbNameCust = "customer";
    String dbNameImdb = "imdb";
    //String driver = "com.mysql.jdbc.Driver";
    String userName = "root";
    String password = "";
    int userId;
    final int maxAmount = 3;
    int amountOpen;
    String username;

    public DatabaseInterAction() {
    }
    public static void main(String[] args) {

        DatabaseInterAction connect = new DatabaseInterAction();

        Scanner sc = new Scanner(System.in);
        boolean validInput = false;

        System.out.println("Create account - press 0");
        System.out.println("Log in - press 1");
        System.out.println("To exit - write 'quit'\n");
        while (!validInput) {
            String input = sc.next().trim();
            System.out.println("user input: " + input);


            switch (input){
                case "0": connect.createAccount();
                    //validInput = true;
                    break;
                case "1": connect.logOn();
                    //validInput = true;
                    break;
                case "quit": System.exit(0);
                default:  System.out.println("Please enter a valid choice.\n");
            }
        }

    }
    public void createAccount(){
        //TODO maybe add field insert restrictions (only letters or something)
        Scanner sc = new Scanner(System.in);

        try {
            //Class.forName(driver);
            Connection conn = DriverManager.getConnection(url+dbNameCust,userName,password);
            System.out.println("-----Connected to MySQL - Customer-----");
            try {

                username = null;
                boolean validUsername = false;

                while(!validUsername){
                    System.out.println("Enter your username: ");
                    username = sc.next().toLowerCase().trim();

                    PreparedStatement user = conn.prepareStatement("SELECT count(login) FROM customer where login = ? ");
                    user.setString(1,username);
                    ResultSet rs = user.executeQuery();
                    if (rs.next()){ //No need for 'while' because only one value in rs
                        if (rs.getInt(1) > 0) {
                            System.out.println("Sorry, this username is taken. Try again!");
                        }
                        else{
                            validUsername = true;
                        }
                    }
                }
                System.out.println("Enter your password: ");
                String password = sc.next().trim();
                System.out.println("Enter your first name: ");
                String name = sc.next().trim();
                System.out.println("Enter your last name: ");
                String lastName = sc.next().trim();

                PreparedStatement user = conn.prepareStatement("insert INTO customer (login,pWd,fName,lName) VALUES (?,?,?,?)",
                        Statement.RETURN_GENERATED_KEYS); //Added the statement, could not find keys without it
                user.setString(1,username);
                user.setString(2,password);
                user.setString(3,name);
                user.setString(4,lastName);

                int affectedRows = user.executeUpdate();

                if (affectedRows == 0) {
                    throw new SQLException("Creating user failed, no rows affected.");
                }
                try (ResultSet generatedKeys = user.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        userId = (int) generatedKeys.getLong(1); //userid was previously this.userId. Why?
                    }
                    else {
                        throw new SQLException("Creating user failed, no ID obtained.");
                    }
                }
            }
            catch(SQLException s){
                System.out.println(s.toString());
            }

            conn.close();
            System.out.println("-----Disconnected from MySQL - Customer-----");
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        this.chooseAction();

    }
    public void logOn(){
        Scanner sc = new Scanner(System.in);

        try {
            //Class.forName(driver);
            Connection conn = DriverManager.getConnection(url+dbNameCust,userName,password);
            System.out.println("-----Connected to MySQL - Customer-----");
            try {
                String password;

                boolean validUsernameAndPassword = false;
                while(!validUsernameAndPassword){
                    System.out.println("Enter your username");
                    username = sc.next().toLowerCase().trim();
                    System.out.println("Enter your password");
                    password = sc.next();

                    PreparedStatement user = conn.prepareStatement("SELECT id FROM customer where login = ? and pWd = ? ");
                    user.setString(1,username);
                    user.setString(2,password);
                    ResultSet rs = user.executeQuery();
                    if(rs.next()){ //Has previously unnecessary extra if clause
                        validUsernameAndPassword = true;
                        userId = rs.getInt(1); //had this
                    }
                    else{
                        System.out.println("Your username and password did not match.");
                        System.out.println("Please try again.");
                    }
                }
                System.out.println("Your UserID: "+this.userId);

            }
            catch(SQLException s){
                System.out.println(s.toString());
            }

            conn.close();
            System.out.println("-----Disconnected from MySQL - Customer-----");
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        this.chooseAction();
    }
    public void chooseAction() {
        System.out.println("Welcome, " + username);
        boolean validInput = false;
        while (!validInput) {
            try {
                Connection connCust = DriverManager.getConnection(url + dbNameCust, userName, password);
                System.out.println("-----Connected to MySQL - Customer-----\n");
                try {
                    PreparedStatement checkOpenAmount = connCust.prepareStatement(
                            "SELECT COUNT(*) " +
                            "FROM rental " +
                            "WHERE cId = ? " +
                            "AND status = 1");

                    checkOpenAmount.setInt(1, userId);
                    ResultSet howManyOpen = checkOpenAmount.executeQuery();
                    howManyOpen.next();
                    amountOpen = howManyOpen.getInt(1);


                    System.out.println("You have rented "+amountOpen+" movies out of "+maxAmount+"\n");

                    Scanner sc = new Scanner(System.in);

                    System.out.println("Search - press 0");
                    System.out.println("Rent - press 1");
                    System.out.println("Return Movie - press 2");




                        String input = sc.next().trim();
                        System.out.println("You entered: " + input);

                        switch (input) {
                            case "0":
                                this.search();
                                break;
                            case "1":
                                this.rent();
                                break;
                            case "2":
                                this.returnMovie();
                                break;
                            case "quit":
                                System.exit(0);
                            default:
                                System.out.println("Please enter a valid choice.");
                        }

                } catch (SQLException e) {
                    System.out.println(e.getMessage());// can someone else do the exception handling? I don't really know what I'm doing
                }

                connCust.close();
                System.out.println("-----Disconnected from MySQL - Customer-----");
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
    }
    public void search(){
        System.out.println("Search");

        Scanner sc = new Scanner(System.in);

        try {
            //Class.forName(driver);
            Connection connCustomer = DriverManager.getConnection(url+dbNameCust,userName,password);
            System.out.println("-----Connected to MySQL - Customer-----");
            Connection connImdb = DriverManager.getConnection(url+dbNameImdb,userName,password);
            System.out.println("-----Connected to MySQL - IMDB-----");
            try {
                System.out.println("What movie are you looking for?");

                String movieName = sc.nextLine().toLowerCase().trim();

                PreparedStatement movies = connImdb.prepareStatement("SELECT title, id FROM movie where title like ?");
                movies.setString(1,"%"+movieName+"%");
                System.out.println("\"%"+movieName +"%\"");
                ResultSet rs = movies.executeQuery();

                while (rs.next()) {
                    int movieId = rs.getInt(2);
                    PreparedStatement data = connCustomer.prepareStatement(
                            "SELECT cId " +
                            "FROM customer.rental " +
                            "WHERE mId = ? " +
                            "AND status = 1 " //Our status is 1 or 0
                    );
                    data.setInt(1, movieId);
                    ResultSet dataset = data.executeQuery();
                    String availability = "available";
                    if (dataset.next())
                    {
                        int otherClient = dataset.getInt(1);
                        if (otherClient == userId )
                            availability = "Rented by current user";
                        else
                            availability= "Rented by another user";
                    }
                    System.out.println("-----------------------------------------");
                    System.out.println("Movie title: "+rs.getString(1)+ " ("+availability+")" );
                    System.out.println("Movie ID:    "+movieId);

                    PreparedStatement people = connImdb.prepareStatement(
                            "SELECT name, role " +
                            "FROM imdb.involved, imdb.person " +
                            "WHERE involved.movieId = ? " +
                            "AND involved.personId = person.id " +
                            "ORDER BY role DESC");

                    people.setInt(1, movieId);
                    ResultSet rs2 = people.executeQuery();

                    while (rs2.next()){
                        String role = rs2.getString(2);
                        String name = rs2.getString(1);
                        System.out.println(role + ": " + name);
                    }
                }


            }
            catch(SQLException s){
                System.out.println(s.toString());
            }

            connCustomer.close();
            System.out.println("-----Disconnected from MySQL - Customer-----");
            connImdb.close();
            System.out.println("-----Disconnected from MySQL - IMDB-----");

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void rent(){
        System.out.println("Rent");
        Scanner sc = new Scanner(System.in);

        try {
            //Class.forName(driver);
            Connection connCust = DriverManager.getConnection(url+dbNameCust,userName,password);
            System.out.println("-----Connected to MySQL - Customer-----");
            Connection connImdb = DriverManager.getConnection(url+dbNameImdb,userName,password);
            System.out.println("-----Connected to MySQL - IMDB-----");

            try {

                PreparedStatement checkMovieStatus = connCust.prepareStatement(
                    "SELECT cId " +
                    "FROM rental " +
                    "WHERE mId = ? " +
                    "AND status = 1");
                PreparedStatement checkMovieExist = connImdb.prepareStatement(
                    "SELECT * " +
                    "FROM movie " +
                    "WHERE id = ?");
                PreparedStatement addMovieToRental = connCust.prepareStatement(
                    "INSERT INTO rental (cId, mId, status) " +
                    "VALUES (?,?,?)");


                if (amountOpen < 3) {

                    System.out.println("You have "+ amountOpen +" unreturned movies.");//Removed in final product
                    boolean validMovie = false;
                    while (!validMovie) {

                        int mId = 0;
                        boolean validRent = false;
                        while (!validRent) {

                            System.out.println("Enter the ID of the movie you would like to rent: ");
                            mId = sc.nextInt();
                            checkMovieStatus.setInt(1, mId);
                            ResultSet isMovieTaken = checkMovieStatus.executeQuery();

                            if (isMovieTaken.next()) {
                                if (isMovieTaken.getInt(1) == userId) {
                                    System.out.println("Sorry! You have already rented this movie!\n");
                                } else {
                                    System.out.println("Sorry! Somebody has already rented this movie!\n");
                                }
                            } else {
                                System.out.println("This movie is not rented\n"); //Final product doesn't have this line
                                validRent = true;
                            }
                        }

                        checkMovieExist.setInt(1, mId);
                        ResultSet doesMovieExist = checkMovieExist.executeQuery();

                        if (doesMovieExist.next()) {
                            System.out.println("You have successfully rented the movie: " + "\n"); //optional: add the name of the movie to feedback
                            addMovieToRental.setInt(1, userId);
                            addMovieToRental.setInt(2, mId);
                            addMovieToRental.setInt(3, 1);
                            addMovieToRental.executeUpdate();
                            //TODO check if data reached db
                            validMovie = true;
                        } else {
                            System.out.println("It seems that the ID you inserted does not ");
                            System.out.println("correspond to any of the available movies.");
                            System.out.println("Please enter a correct movie ID:\n ");
                        }
                    }
                }
                else{
                    System.out.println("You have "+ amountOpen +" unreturned movies.");
                    System.out.println("Return some movies to be able to rent new ones.\n");
                }
            }

            catch (Exception e) {
                System.out.println(e.getMessage());// can someone else do the exception handling?
            }

            connCust.close();
            System.out.println("-----Disconnected from MySQL - Customer-----");
            connImdb.close();
            System.out.println("-----Disconnected from MySQL - IMDB-----");

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void returnMovie(){
        System.out.println("Return movie\n");
        Scanner sc = new Scanner(System.in);
        int mId = 0;

        try {
            //Class.forName(driver);
            Connection connCust = DriverManager.getConnection(url+dbNameCust,userName,password);
            System.out.println("-----Connected to MySQL - Customer-----");
            try {
                PreparedStatement checkIfMovieOpen = connCust.prepareStatement(
                "SELECT * " +
                "FROM rental " +
                "WHERE cId = ? " +
                "AND mId = ? " +
                "AND status = 1");
                PreparedStatement closeMovieRent = connCust.prepareStatement(
                "UPDATE rental SET status = 0 " +
                "WHERE cId = ? " +
                "AND mId = ?");

                System.out.println("What movie do you want to return?");
                mId = sc.nextInt();

                checkIfMovieOpen.setInt(1, userId);
                checkIfMovieOpen.setInt(2, mId);
                ResultSet isMovieRented = checkIfMovieOpen.executeQuery();

                if(isMovieRented.next()){
                    closeMovieRent.setInt(1, userId);
                    closeMovieRent.setInt(2, mId);
                    closeMovieRent.executeUpdate();
                    //connCust.commit();
                    //TODO check if update made?
                    System.out.println("Movie has been successfully returned."); //add movie name?
                }
                else{
                    System.out.println("You have not rented this movie.");
                    System.out.println("You cannot return it.\n");
                }
            }
            catch(SQLException s){
                System.out.println(s.toString());
            }

            connCust.close();
            System.out.println("-----Disconnected from MySQL - Customer-----");
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

}
