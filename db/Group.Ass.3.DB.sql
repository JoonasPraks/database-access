CREATE DATABASE `customer` /*!40100 DEFAULT CHARACTER SET utf8 */;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) DEFAULT NULL,
  `pWd` varchar(20) DEFAULT NULL,
  `fName` varchar(20) DEFAULT NULL,
  `lName` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `rental` (
  `cId` int(11) DEFAULT NULL,
  `mId` int(11) DEFAULT NULL,
  `status` bit(1) DEFAULT NULL,
  `transId` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`transId`),
  KEY `cId` (`cId`),
  KEY `rental_ibfk_2_idx` (`mId`),
  CONSTRAINT `rental_ibfk_1` FOREIGN KEY (`cId`) REFERENCES `customer` (`id`),
  CONSTRAINT `rental_ibfk_2` FOREIGN KEY (`mId`) REFERENCES `imdb`.`movie` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
